"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const koa = require("koa");
const Router = require("koa-router");
const user_1 = require("./controller/user");
const app = new koa();
const router = new Router();
router.get('/', (ctx) => {
    ctx.body = {
        a: 1
    };
});
app.use(router.routes());
app.use(user_1.default.routes());
app.listen(3000);
console.log('i am listrning 3000');
//# sourceMappingURL=server.js.map