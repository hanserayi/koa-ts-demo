import * as koa from 'koa'
import * as Router from 'koa-router'
import userRouter from './controller/user'

const app = new koa()
const router = new Router()

router.get('/', (ctx: koa.Context) => {
  ctx.body = {
    a: 1
  }
})

app.use(router.routes())
app.use(userRouter.routes())

app.listen(3000)
console.log(
  'i am listrning 3000'
);
